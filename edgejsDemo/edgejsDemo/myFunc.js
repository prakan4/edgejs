﻿return function (data, callback) {
    var result = "nothing";
    //callback(null, 'Version: ' + process.env.EDGE_NATIVE + 'Platform'+ process.platform);

    /**
 * This file creates a highchart, 
 * no html page is required.The html is crafted
        * within this script.
 * */
    const puppeteer = require('puppeteer')
    const fs = require('fs')

    async function run() {

        var dim;
        const browser = await puppeteer.launch({
            headless: true
        })
        // const browser = await puppeteer.launch({
        //     headless: false,
        //     slowMo: 2000,
        //     devtools: true
        // })

        const page = await browser.newPage()

        const loaded = page.waitForNavigation({
            waitUntil: 'load'
        })

        const html =

            `<!DOCTYPE html>
        <html lang="en">
            <head>
                <meta charset="UTF-8">
                <title>Highcharts Test 4</title>
            </head>
            <body>
                <div id="container" style="width:100%; height:400px;">testInsideContainer</div>
            </body>
        </html>`

        await page.setContent(html)
        await loaded

        async function loadChart() {
            page.evaluate(fs.readFileSync('C:\\ArcherCode\\AP\\ArcherSolutions\\Archer.Web\\SECURITY2000\\apps\\common\\node_modules\\highcharts\\highcharts.js', 'utf8'));
            await page.evaluate(async (fs) => {

                console.log('page.evaluate Highcharts.version=' + Highcharts.version)

                var myChart = Highcharts.chart('container', {
                    chart: {
                        type: 'bar'
                    },
                    title: {
                        text: 'Key Stats'
                    },
                    xAxis: {
                        categories: ['Goals', 'Assists', 'Key Passes']
                    },
                    yAxis: {
                        title: {
                            text: 'Goal contributions'
                        }
                    },
                    plotOptions: {
                        series: {
                            animation: false
                        }
                    },
                    series: [{
                        name: 'M.Rashford',
                        data: [6, 5, 4]
                    }, {
                        name: 'A.Martial',
                        data: [9, 4, 6]
                    }]
                });
            }, fs)

            dim = await page.evaluate(() => {
                return {
                    width: document.documentElement.clientWidth,
                    height: document.documentElement.clientHeight,
                    deviceScaleFactor: window.devicePixelRatio,
                    containerText: document.getElementById('container').innerHTML
                };
            });
        }

        await loadChart()
        callback(null, dim.containerText);
        await new Promise(resolve => setTimeout(resolve, 50000));
        await browser.close()
    }

    run()

    }

    
