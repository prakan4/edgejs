﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Web;
using EdgeJs;

namespace edgejsDemo
{
    internal class Program
    {
        public static void Start()
        {
            string buildPath = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            string JSfilePath = Path.Combine(buildPath, "myFunc.js");
            var func = Edge.Func(File.ReadAllText(JSfilePath));
            var funcTask = func(".NET test string");
            var result = funcTask.Result;
            Console.WriteLine(result);
            Console.ReadLine(); 
        }

        static void Main(string[] args)
        {
            Start();
        }
    }
}
